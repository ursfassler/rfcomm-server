/*
 * (C) Copyright 2016 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "Client.hpp"

#include <unistd.h>
#include <iostream>


Client::Client(int _fd) :
  fd{_fd}
{
}

Client::~Client()
{
  ::close(fd);
}

std::vector<std::uint8_t> Client::read()
{
  std::uint8_t buf[1024];
  int bytes_read = ::read(fd, buf, sizeof(buf)-1);
  if (bytes_read > 0) {
    return std::vector<std::uint8_t>{buf, buf+bytes_read};
  }
  return {};
}

void Client::write(const std::vector<uint8_t> &message)
{
  int ret = ::write(fd, message.data(), message.size());
}
