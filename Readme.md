RFCOMM Server
=============

Serial over Bluetooth server for Linux.

Usage
-----

Run the server as root.

Connection problem
------------------

If the connection fails, try

    sdptool browse local

If it returns something like

    Failed to connect to SDP server on FF:FF:FF:00:00:00: Connection refused

then add the `--compat` flag in the bluetooth service file and restart the daemon and service.

I.e. in the file `/etc/systemd/system/bluetooth.target.wants/bluetooth.service` (Debian), change the corresponding line to

    ExecStart=/usr/lib/bluetooth/bluetoothd --compat

and

    systemctl daemon-reload
    systemctl restart bluetooth.service
