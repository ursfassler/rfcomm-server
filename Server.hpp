/*
 * (C) Copyright 2016 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include <bluetooth/sdp.h>
#include <bluetooth/sdp_lib.h>


class Server
{
  public:
    Server();
    ~Server();

    int accept();

  private:
    int s;
    sdp_session_t *session;

    sdp_session_t *register_service();
};
