/*
 * (C) Copyright 2016 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#pragma once

#include <string>
#include <vector>
#include <cstdint>


class Client
{
  public:
    Client(int fd);
    ~Client();

    std::vector<uint8_t> read();
    void write(const std::vector<std::uint8_t> &message);

  private:
    const int fd;
};
