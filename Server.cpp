/*
 * (C) Copyright 2016 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "Server.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/socket.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>

#include <bluetooth/sdp.h>
#include <bluetooth/sdp_lib.h>
#include <bluetooth/rfcomm.h>

#include <iostream>


#undef BDADDR_ANY
static const bdaddr_t _BDADDR_ANY{0, 0, 0, 0, 0, 0};
static auto BDADDR_ANY{&_BDADDR_ANY};

#undef BDADDR_LOCAL
static const bdaddr_t _BDADDR_LOCAL{0, 0, 0, 0xff, 0xff, 0xff};
static auto BDADDR_LOCAL{&_BDADDR_LOCAL};


Server::Server()
{
  session = register_service();
  s = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);

  struct sockaddr_rc loc_addr = { 0, _BDADDR_ANY, 0 };
  loc_addr.rc_family = AF_BLUETOOTH;
  loc_addr.rc_bdaddr = *BDADDR_ANY;
  loc_addr.rc_channel = (uint8_t) 1;

  std::cout << "bind" << std::endl;
  if (bind(s, (struct sockaddr *)&loc_addr, sizeof(loc_addr)) < 0) {
    throw std::runtime_error("bind error");
  }

  std::cout << "listen" << std::endl;
  if (listen(s, 1) < 0) {
    throw std::runtime_error("listen error");
  }
}

Server::~Server()
{
  close(s);
  sdp_close(session);
}

int Server::accept()
{
  struct sockaddr_rc rem_addr = { 0, _BDADDR_ANY, 0 };
  socklen_t opt = sizeof(rem_addr);

  std::cout << "accept" << std::endl;
  const auto client = ::accept(s, (struct sockaddr *)&rem_addr, &opt);
  if (client < 0) {
    throw std::runtime_error("accept error");
  }

  char buf[20] = { 0 };
  ba2str(&rem_addr.rc_bdaddr, buf);
  std::cout << "accepted connection from " << buf << std::endl;

  return client;
}

sdp_session_t *Server::register_service()
{
    uint32_t svc_uuid_int[] = { 0x00000000,0x00000000,0x00000000,0x78563412 };
//  uint32_t svc_uuid_int[] = { 0x299df394, 0x7d436d7d, 0xa3fb3b97, 0xeed4499e };
//  94f39d29-7d6d-437d-973b-fba39e49d4ee

  uint8_t rfcomm_channel = 1;
  const char *service_name = "rfcomm-server";
  const char *service_dsc = "Serial over Bluetooth server for Linux";
  const char *service_prov = "Urs Fässler";

  uuid_t root_uuid, l2cap_uuid, rfcomm_uuid, svc_uuid, svc_id;
  sdp_list_t *l2cap_list = 0,
      *rfcomm_list = 0,
      *root_list = 0,
      *proto_list = 0,
      *access_proto_list = 0,
      *profile_list = 0;
  sdp_data_t *channel = 0;

  sdp_record_t *record = sdp_record_alloc();

  // set the general service ID
  sdp_list_t *service_class = 0;

  sdp_uuid128_create( &svc_uuid, &svc_uuid_int );
  sdp_set_service_id( record, svc_uuid );
  service_class = sdp_list_append(service_class, &svc_uuid);

  sdp_uuid16_create(&svc_id, SERIAL_PORT_SVCLASS_ID);
  sdp_set_service_id( record, svc_id );
  service_class = sdp_list_append(service_class, &svc_id);

  sdp_set_service_classes( record, service_class);

  // make the service record publicly browsable
  sdp_uuid16_create(&root_uuid, PUBLIC_BROWSE_GROUP);
  root_list = sdp_list_append(0, &root_uuid);
  sdp_set_browse_groups( record, root_list );

  // set l2cap information
  sdp_uuid16_create(&l2cap_uuid, L2CAP_UUID);
  l2cap_list = sdp_list_append( 0, &l2cap_uuid );
  proto_list = sdp_list_append( 0, l2cap_list );

  // set rfcomm information
  sdp_uuid16_create(&rfcomm_uuid, RFCOMM_UUID);
  channel = sdp_data_alloc(SDP_UINT8, &rfcomm_channel);
  rfcomm_list = sdp_list_append( 0, &rfcomm_uuid );
  sdp_list_append( rfcomm_list, channel );
  sdp_list_append( proto_list, rfcomm_list );

  // attach protocol information to service record
  access_proto_list = sdp_list_append( 0, proto_list );
  sdp_set_access_protos( record, access_proto_list );

  // profile

  uuid_t psmValue;
  sdp_uuid16_create(&psmValue, SERIAL_PORT_SVCLASS_ID);

  sdp_profile_desc_t profile_desc;
  profile_desc.version = 0x0100;
  profile_desc.uuid = psmValue;
  profile_list = sdp_list_append( profile_list, &profile_desc );

  sdp_set_profile_descs(record, profile_list);

  // set the name, provider, and description
  sdp_set_info_attr(record, service_name, service_prov, service_dsc);

  // connect to the local SDP server, register the service record, and
  // disconnect
  auto session = sdp_connect( BDADDR_ANY, BDADDR_LOCAL, SDP_RETRY_IF_BUSY );
  if (!session) {
    throw std::runtime_error("could not connect to SDP server");
  }
  const auto err = sdp_record_register(session, record, 0);

  // cleanup
  sdp_record_free(record);
  sdp_data_free( channel );
  sdp_list_free( service_class, 0 );
  sdp_list_free( l2cap_list, 0 );
  sdp_list_free( rfcomm_list, 0 );
  sdp_list_free( root_list, 0 );
  sdp_list_free( access_proto_list, 0 );
  sdp_list_free( profile_list, 0 );
  sdp_list_free( proto_list, 0 );

  return session;
}
