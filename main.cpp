/*
 * (C) Copyright 2016 Urs Fässler
 *
 * SPDX-License-Identifier: GPLv3
 */

#include "Server.hpp"
#include "Client.hpp"

#include <iostream>
#include <vector>
#include <cstdint>
#include <iomanip>
#include <cmath>


std::ostream& operator <<(std::ostream &stream, const std::vector<std::uint8_t> &data)
{
  stream << std::hex;
  for (const std::uint8_t item : data) {
    stream << " ";
    stream.fill('0');
    stream.width(2);
    stream << (int)item;
  }
  stream << std::dec;
  return stream;
}

int main()
{
  Server server;

  while (true) {
    Client client{server.accept()};

    while (true) {
      const auto message{client.read()};
      if (message.empty()) {
        break;
      }
      std::cout << "read: " << message << std::endl;
    }
  }

  return 0;
}
